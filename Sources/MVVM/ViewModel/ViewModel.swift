//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import Foundation
import Combine

open class ViewModel: ViewModelable {

    // MARK: - properties

    public var cancellables: Set<AnyCancellable> = []

    public init() {

    }
}
