//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import Foundation
import Combine

public protocol ControllerViewModelable: ViewModelable {
    typealias NavBarSideItemsSubject = CurrentValueSubject<[BarButtonItem.ViewModel], Never>

    var navBarLeftItems: NavBarSideItemsSubject { get }
    var navBarRightItems: NavBarSideItemsSubject { get }
}
