//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import Foundation

open class ControllerViewModel: ViewModel,
                                ControllerViewModelable {

    // MARK: - properties

    public private(set) var navBarLeftItems: NavBarSideItemsSubject = .init([])

    public private(set) var navBarRightItems: NavBarSideItemsSubject = .init([])
}
