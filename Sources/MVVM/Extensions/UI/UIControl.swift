//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import UIKit
import Combine

public extension UIControl {
    struct Action {
        public typealias Completion = () -> Void

        public let event: UIControl.Event
        public let completion: Completion

        public static func touchUpInside(completion: @escaping Completion) -> Action {
            .init(event: .touchUpInside, completion: completion)
        }

        public func callAsFunction() {
            completion()
        }
    }
}

// https://betterprogramming.pub/observe-uibutton-events-using-combine-in-swift-5-63c1a4e0a0c1
public extension UIControl {
    private class InteractionSubscription<
        SubscriberType: Subscriber,
        Control: UIControl
    >: Subscription where SubscriberType.Input == Control {
        private var subscriber: SubscriberType?
        private let control: Control

        init(
            subscriber: SubscriberType,
            control: Control,
            event: UIControl.Event
        ) {

            self.subscriber = subscriber
            self.control = control

            self.control.addTarget(self, action: #selector(handleEvent), for: event)
        }

        @objc
        private func handleEvent(_ sender: UIControl) {
            _ = self.subscriber?.receive(self.control)
        }

        func request(_ demand: Subscribers.Demand) {}

        func cancel() {
            self.subscriber = nil
        }
    }

    struct InteractionPublisher<Control: UIControl>: Publisher {
        public typealias Output = Control
        public typealias Failure = Never

        private let control: Control
        private let events: UIControl.Event

        init(control: Control, events: UIControl.Event) {
            self.control = control
            self.events = events
        }

        public func receive<S: Subscriber>(subscriber: S) where S.Failure == Self.Failure, S.Input == Self.Output {
            let subscription = InteractionSubscription(
                subscriber: subscriber,
                control: control,
                event: events
            )

            subscriber.receive(subscription: subscription)
        }
    }

    func publisher(for events: UIControl.Event) -> InteractionPublisher<UIControl> {
        return InteractionPublisher(control: self, events: events)
    }
}
