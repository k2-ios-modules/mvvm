//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import Foundation
import Combine

public protocol MVVMEntity: AnyObject {
    associatedtype ViewModel: ViewModelable

    var cancellables: Set<AnyCancellable> { get set }
    var viewModel: ViewModel { get }

    init(vm: ViewModel)

    func setBinding()
}
