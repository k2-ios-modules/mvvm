//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import UIKit

extension BarButtonItem {
    open class ViewModel: MVVM.ViewModel {

        // MARK: - properties

        @Published
        public var image: UIImage?
        @Published
        public var action: UIControl.Action?

        // MARK: - init

        public override init() {
            super.init()
        }

        public init(
            image: UIImage?,
            action: UIControl.Action?
        ) {
            self.image = image
            self.action = action
        }
    }
}
