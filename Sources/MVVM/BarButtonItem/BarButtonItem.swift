//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
// 

import UIKit
import Combine

open class BarButtonItem: UIBarButtonItem {

    // MARK: - variables

    public var cancellables: Set<AnyCancellable> = []

    // MARK: - init

    public static func make() -> BarButtonItem {
        let item = BarButtonItem(customView: UIButton())
        item.imageInsets = .zero
        return item
    }

    // MARK: - setters

    public final func setButtonAction(_ action: UIControl.Action?) {
        guard let button = self.button,
              let action
        else { return }

        button
            .publisher(for: action.event)
            .sink(receiveValue: { _ in
                action.completion()
            })
            .store(in: &self.cancellables)
    }

    public final func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()
        vm.$action
            .sink { [weak self] in self?.setButtonAction($0) }
            .store(in: &self.cancellables)
        vm.$image
            .sink { [weak self] in self?.button?.setImage($0, for: .init()) }
            .store(in: &self.cancellables)
    }
}
