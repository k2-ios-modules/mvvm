// swift-tools-version: 5.7

import PackageDescription

let package = Package(
    name: "MVVM",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "MVVM",
            targets: ["MVVM"]
        ),
    ],
    targets: [
        .target(
            name: "MVVM"
        )
    ]
)
